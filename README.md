# KIT IoT im Büro Webseite


## Mit GitLab editieren

Die statischen Seiten dieses Projekts werden von der [GitLab CI][ci] automatisch erstellt und kann momentan auf http://office-iot.teco.edu angeschaut werden. Über den dort angezeigten Editierknopf kann sie auch direkt editiert werden (der eigentliche Quellcode der Seite befindet sich [/docs](/docs) Ordner). Die Seite verwendet [Markdown Syntax][markdown] und [MkDocs][mkdocs]. Für das verwendete Material Theme gibt es eine zusätzliche [Referenz][reference]. Es wird über https://gitlab.kit.edu/kit/teco/software/traefik-gitlab-pages/ gehosted (falls mal was nicht neu laden sollte oder es Serverfehler gibt, bitte dort melden)

##  Lokal editieren

Um mit diesem Projekt lokal zu arbeiten (kann bei größeren Änderungen Sinn machen), müssen Sie die folgenden Schritte ausführen:

1. Forken, klonen oder downloaden Sie dieses Projekt
1. [Installieren][install] Sie MkDocs mit dem Material theme
1. Vorschau auf Ihr Projekt: `mkdocs serve`,
   Ihre Seite kann unter `localhost:8000` erreicht werden
1. Inhalt hinzufügen
1. Erzeugen Sie die Website: `mkdocs build` (optional)


[ci]: https://about.gitlab.com/gitlab-ci/
[mkdocs]: http://www.mkdocs.org
[install]: https://squidfunk.github.io/mkdocs-material/getting-started/
[reference]: https://squidfunk.github.io/mkdocs-material/reference/
[markdown]: https://de.wikipedia.org/wiki/Markdown
