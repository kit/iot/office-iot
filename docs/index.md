# Willkommen bei IoT im Büro

Ziel des Projektes ist die Befähigung von KIT-Mitglieder zur bedarfsgerechten 
Gestaltung Ihres Arbeitsplatzes mit Hilfe von intelligenten Geräten.

Neben der zentralen Infrastruktur soll das Projekt in Zeiten von Klimawandel 
und New Work, helfen z.B. das Raumklima zu verbessern und Ressourcen einzusparen.
Aber auch andere Anwendungsbereiche von der Arbeitssicherheit bis hin zur intelligenten
Institutsküche können unterstützt werden. Die Anleitungen sollen idealerweise eine Hilfestellung zu einem 
mit dem Datenschutz und dem Gebäudemanagement abgestimmten Vorgehen liefern und Erfolgsbeispiele und Best Practice dokumentieren.

<div class="grid cards" markdown>

-   :material-clock-fast:{ .lg .middle } __In 10 Minuten loslegen__

    ---

    Einfache Installation und Erste Schritte

    [:octicons-arrow-right-24: Erste Schritte](start.md)

-   :material-ceiling-fan-light:{ .lg .middle } __Erfolgreiche Anwendungen__

    ---

    Verschiedene erfolgreiche IoT-Anwendungen am KIT

    [:octicons-arrow-right-24: Anwendungen](apps.md)

-   :fontawesome-solid-blender-phone:{ .lg .middle } __Geräte und Sensoren fürs Büro__

    ---

    Nützliche Geräte und Sensoren für die dezentrale Beschaffung

    [:octicons-arrow-right-24: Geräte](devices.md)

-   :fontawesome-solid-book-atlas:{ .lg .middle } __Detailierte Dokumentation__

    ---

    Dokumentation und Links zur eingesetzten Software

    [:octicons-arrow-right-24: Software-Dokumentation](docs.md)

</div>


Diese Seite wird über das [KIT GitLab](https://gitlab.kit.edu/) gehostet und ist editierbar, doku hierzu unter [mkdocs.org](http://mkdocs.org) und dem genutzten Theme [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/).