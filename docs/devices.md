# Geräte

| Gerätename | Typ/Modell | Produzent | Notizen | Link
|------------|------------|------------|-------------|-----------|
| Luftfeuchtigkeits- und Temperatursensor | Aqara T1 | Aqara | Funktioniert sehr gut | Rack 3/RZ1 |