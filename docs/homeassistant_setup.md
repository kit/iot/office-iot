# Introduction
The telecooperation Office (TECO) of the Karsruhe Institute of Technology (KIT) recently deployed a Home Assistant instance in order to automate processes and collect environmental data of the offices and hardware labs alike. One primary goal was to make the TECO more energy efficient. For this the consumed total heater energy of the minol counter is recorded, smart plugs can switch not needed electronics, and window sensors can inform staff to either close windows, if forgotten, or to engage ventilation, in case of poor air quality or interior heat, reducing cooling expenses. In the future the opening of windows can be automated by mounting window openers in tactical locations. Even tasks such as fire alarm drill routines and door bell notifications are implemented. 

All in all the possibilities of the Home Assistance for the TECO are plentiful.

# The current setup
The heart of the current setup is one supervised Home Assistant instance, named the "TECOpticon", on one Proxmox managed Debian virtual machine. It serves as administration instance overseeing all processes, data collection, and testing of new features. Naturally, only authorized personnel is to be given access to this Panopticon. At the present time TECOpticon hosts the MQTT broker (Mosquitto Broker).

On a secondary Proxmox Ubuntu virtual machine lies a MariaDB database to store all sensor data. This VM can also be used to hosts auxiliary scripts to perform various other tasks, such as reading the current Minol counter value.

The tertiary part is a Raspberry Pi, located in the meeting room, hosting Zigbee2MQTT, which as the name suggests relays Zigbee sensor data to MQTT to TECOpticon. The Docker Compose install of Zigbee2MQTT is used.

The structure can be visualized as such:
```plaintext
Proxmox
├── Debian VM
│   └── Home Assistant (TECOpticon)
│       └── MQTT Broker (Mosquitto Broker)
├── Ubuntu VM
│   ├── MariaDB
│   └── Minol Reader (script) → MQTT
Raspberry Pi
└── Zigbee2MQTT
    └── Zigbee → MQTT
```

# Expanding the system
You want to expand the Home Assistant system?
Great! Here is a guide that (hopefully) will help you achieve exactly that.

Furthermore, there are some maintenance information about the other parts of cheese the current setup, should there ever be the need of change or redo any part of it.

## Home Assistant instances
Short guide:
- Make **Debian** [Proxmox](https://proxmox.teco.edu) VM
	- Must be at least Debian 12 aka Bookworm (no derivatives)
	- 4GB RAM; (min.) 16GB disk space
	- USE [VM GUIDE](https://intranet.teco.edu/projects/admin/wiki/VirtualMachines)
		- READ "Checklist" OR BRICK THE VM!
		- Ask Sirus how to properly start/stop and force stop a VM
	- `sudo apt update && sudo apt upgrade`
- Install Home Assistant Supervised: [Installer Repo](https://github.com/home-assistant/supervised-installer)
	- Install Dependencies
	- Install Docker-CE
	- Install OS-Agent: [Repo](https://github.com/home-assistant/os-agent/tree/main#using-home-assistant-supervised-on-debian)
		- latest `os-agent_X.X.X_linux_x86_64.deb` must be manually downloaded: [latest](https://github.com/home-assistant/os-agent/releases)
		- (`libglib2.0-bin` may need to be installed; OS-Agent read installation!)
	- Install Home Assistant Supervised Debian Package
- Start and Login to Home Assistant
	- Make new account
		- If backup, load from backup
	- Install HACS 
		- Just watch tutorial [Youtube](https://youtu.be/Q8Gj0LiklRE?si=PMW2YTDFqt9CTCse)
		- Install "button-card"
	- Install Addons:
		- File editor
		- Mosquitto broker
			- Just watch tutorial [Youtube](https://www.youtube.com/watch?v=dqTn-Gk4Qeo)
		- MQTT Explorer
		- Zigbee2MQTT Proxy
			- Add address to Zigbee2MQTT instance
		- ESPHome
		- Studio Code Server
	- Setup Recorder (if DB is already available)
		- [Home Assistant Recorder](https://www.home-assistant.io/integrations/recorder/)

In order to get the most of Home Assistant the Supervised version must be installed. For this all requirements of the installation guide **must** be rigorously fulfilled or else unexpected errors may occur later. Start here with the [Proxmox](https://proxmox.teco.edu) VM. You (currently) need at least a Debian 12 aka Bookworm (no derivatives) Debian distribution. Give the VM 4GB RAM and about 16GB disk space. For the installation consult with your local Sirius and read the [VM GUIDE](https://intranet.teco.edu/projects/admin/wiki/VirtualMachines) on the TECO Intranet how to setup the VM. Note that it is VERY easy to brick the VM during setup if you don't adhere to the setup steps. Update and upgrade at the end.

Once the VM is setup go to the [Repo](https://github.com/home-assistant/supervised-installer) of the Supervised Home Assistant version. Here follow the installation steps. For installing the dependencies and the Docker-CE use the provided install commands. For installing the OS-Agent a little bit extra work is needed. Go to the OS-Agent [Repo](https://github.com/home-assistant/os-agent/tree/main#using-home-assistant-supervised-on-debian) and follow the guide. Download the latest `os-agent_X.X.X_linux_x86_64.deb` for the installation and if an error occurs at the last step you may need to manually install the `libglib2.0-bin` with `sudo apt install -y libglib2.0-bin`. Installing the Supervised Home Assistant version itself is again summed up in a single command.

If you encounter trouble with home assistant not being able to connect to the internet, make sure that Ethernet connection is managed with the network manager!

Last but not least the setup of Home Assistant itself. If you want to create a replication of the TECOpticon instance or want to recreate another Home Assistant instance, you can load a backup. Although it is recommended to first normally login and check if Home Assistant runs normally without issues. If Home Assistant does not have internet connection, loading a backup will stall indefinitely!
If you want to create a generic Home Assistant instance, don't load from a backup and install the above listed add-ons manually. This should not take too long. Not all add-ons are necessary, the only necessary for operation is the Zigbee2MQTT Proxy. HACS can be omitted but it is useful for more customization options (especially with "button-card"). The File editor add-on should be installed if the `configuration.yaml` needs to be configured.

If there is already an external database (such as MariaDB), the recorder integration can be used to save all data in the database. To do this open the `configuration.yaml` in the File editor and add the recorder integration. Information about the recorder integration can be found on the [Recorder Documentation](https://www.home-assistant.io/integrations/recorder/).

Here is a template of how it may look like:
```yaml
# Recorder for Database
recorder:
  db_url: !secret mariadb_url
  auto_purge: false
  commit_interval: 60
  purge_keep_days: 3650 # Set to 10 years
  exclude: # taken from an example; can be changed later; domain: - history_graph - group may be excluded
    domains:
      - media_player
      - scan_store
      - zone
      - zwave
    entities:
      - sensor.date
      - sensor.time
```

The `db_url` contains the password and therefore should be stored in the `secret.yaml`.

The `db_url` in `secret.yaml` may look like this:
```yaml
mariadb_url: "mysql://homeassistant:PASSWORD@IP/homeassistant?charset=utf8mb4"
```
Replace PASSWORD and IP respectively. Additionally make sure that user and database name are set accordingly as well.

## Database VM
Short guide:
- Make **Ubuntu** [Proxmox](https://proxmox.teco.edu) VM
	- min. 4GB RAM; (min.) 32GB disk space
	- USE [VM GUIDE](https://intranet.teco.edu/projects/admin/wiki/VirtualMachines)
		- READ "Checklist" OR BRICK THE VM!
		- Ask Sirus how to properly start/stop and force stop a VM
	- `sudo apt update && sudo apt upgrade`
	- Do major updates till on newest version: [Update Guide](https://itsfoss.com/upgrade-ubuntu-version/)
- Install Docker-CE
	- Either command from Home Assistant setup can used
	- Or follow normal installation: [Docker Install](https://docs.docker.com/engine/install/ubuntu/) (recommended)
- Install MariaDB and Adminer with Docker Compose
	- Example with just MariaDB [Template](https://www.beekeeperstudio.io/blog/how-to-use-mariadb-with-docker)
	- Add just Adminer from [here](https://hub.docker.com/_/mariadb) 
		- Adminder lets you view DB
	- Add necessary credentials to docker-compose
		- Usually:
			- MYSQL_DATABASE: homeassistant
			- MYSQL_USER: homeassistant
		- Passwords can be chosen as seen fit

The setup of the Database [Proxmox](https://proxmox.teco.edu) VM starts of similar to the setup of the Home Assistant VM. In this case, however, Ubuntu is chosen as OS. Give the VM a minimum of 4GB RAM and minimum of 32GB disk space. For the installation consult with your local Sirius and read the [VM GUIDE](https://intranet.teco.edu/projects/admin/wiki/VirtualMachines) on the TECO Intranet how to setup the VM. Note that it is VERY easy to brick the VM during setup if you don't adhere to the setup steps. Aside from the usual update and upgrade, do as many major updates as needed to get to the newest LTS version.

The Docker-CE can be either installed with the single line command from the Home Assistant Supervised ([Installer Repo](https://github.com/home-assistant/supervised-installer)) install or via the normal ubuntu installation from the docker website: [Docker Install](https://docs.docker.com/engine/install/ubuntu/). It is recommended to do the slightly longer Docker Install with the guide on the docker website.

Installing MariaDB is then done with Docker Compose. Whereas the docker compose file can be made by taking the MariaDB [Template](https://www.beekeeperstudio.io/blog/how-to-use-mariadb-with-docker) and adding the Adminer from the [Docker Hub](https://hub.docker.com/_/mariadb). Adminer is used to provide a visual interface to view the database.

Here is an example `docker-compose.yml`:
```yml
version: '3.8'
volumes:
  data:
services:
  db:
    image: mariadb
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: ROOTPASSWORD
      MYSQL_DATABASE: homeassistant
      MYSQL_USER: homeassistant
      MYSQL_PASSWORD: PASSWORD
    volumes:
      - data:/var/lib/mysql
    ports:
      - "3306:3306"

  adminer:
    image: adminer
    restart: always
    ports:
      - 8080:8080
```
Add a PASSWORD and ROOTPASSWORD!
Don't let the password be "PASSWORD".

## Zibee2MQTT
Short guide:
- Get Raspberry Pi 3 (or better)
- Get SONOFF Zigbee 3.0 USB Dongle Plus ZBDongle-P
- Setup Raspberry Pi
	- Install [Raspberry Pi Imager](https://www.raspberrypi.com/software/)
	- Flash "Raspberry Pi OS Lite" (Desktop is not needed)
	- Tipp: Already flash with SSH key
	- Tipp: Find IP of Pi with "Advance IP Scanner" (only windows)
	- `sudo apt update && sudo apt upgrade`
- Install Docker-CE
	- Follow install [Guide](https://docs.docker.com/engine/install/debian/)
- Install Zigbee2MQTT with docker-compose
	- The example can be taken from [Zigbee2MQTT Getting Started](https://www.zigbee2mqtt.io/guide/getting-started/#installation)
		- Fine full path to USB-Zigbee Adapter
		- Add `docker-compose.yml`
		- Make directory `zigbee2mqtt-data`
		- In directory make `configuration.yaml`
			- The adapter is: `zstack`
		

For Zigbee2MQTT a Raspberry Pi 3 (or better) is needed together with a Zigbee Adapter. It is recommended to use the SONOFF Zigbee 3.0 USB Dongle Plus ZBDongle-P. Note there is the ZBDongle-P and the ZBDongle-E. DON'T use the ZBDongle-E! With the [Raspberry Pi Imager](https://www.raspberrypi.com/software/) you can prepare an SD card with the "Raspberry Pi OS Lite", a Desktop environment is not required. Already set an SSH key to make connecting easier and utilize an IP scanner (if needed) to locate the Raspberry Pi in the network. Update and upgrade at the end.

Next on the list is installing the Docker-CE. For this use the Debian [Guide](https://docs.docker.com/engine/install/debian/) on the docker website. The installation of the Docker-CE should be straight forward at this point.

Finally install Zigbee2MQTT using docker-compose with the help of the [Zigbee2MQTT Getting Started](https://www.zigbee2mqtt.io/guide/getting-started/#installation). However, there is a litte bit more work involved rather than just creating a docker-compose file. In the same directory of the `docker-compose.yml` create the directory `zigbee2mqtt-data`, in which a `configuration.yaml` has to be created.

Here is an example of the `docker-compose.yml`:
```yml
services:
  zigbee2mqtt:
    container_name: zigbee2mqtt
    image: koenkk/zigbee2mqtt
    restart: unless-stopped
    volumes:
      - ./zigbee2mqtt-data:/app/data
      - /run/udev:/run/udev:ro
    ports:
      # Frontend port
      - 8080:8080
    environment:
      - TZ=Europe/Berlin
    devices:
      - /dev/serial/by-id/NAME:/dev/ttyUSB0
```
Replace NAME with the id-name of the USB device. This can be found with the command: `ls -la /dev/serial/by-id/`. Make sure that the `ttyUSB0` port is correct.

Here is an example of the `configuration.yaml`:
```yaml
# Let new devices join our zigbee network
permit_join: true
# We want to use it with homeassistant
homeassistant: true
# A web frontend should be provided
frontend: true
# Docker Compose makes the MQTT-Server available using "mqtt" hostname
mqtt:
  base_topic: zigbee2mqtt
  server: MQTT_URL
  user: MQTT_USER
  password: MQTT_PASSWORD
  keepalive: 60
  reject_unauthorized: true
# Zigbee Adapter path
serial:
  port: /dev/ttyUSB0
  adapter: zstack
# Enable the Zigbee2MQTT frontend
frontend:
  port: 8080
# Let Zigbee2MQTT generate a new network key on first start
advanced:
  network_key: GENERATE
```
Replace MQTT_URL, MQTT_USER, MQTT_PASSWORD accordingly. The base topic should remain as `zigbee2mqtt`.
The adapter is set to `zstack`.