# Heizungssteuerung mit Home Assistant

Die Heizungssteuerung mit Home Assistant ermöglicht dir, die Temperatur in deinem Zuhause basierend auf Zeitplänen, Anwesenheitserkennung oder individuellen Präferenzen zu automatisieren. So kannst du nicht nur den Komfort steigern, sondern auch den Energieverbrauch optimieren.

![Beispielbild Heizungssteuerung](./images/heizungssteuerung_example.png)

---

## Benötigte Hardware

Um die Heizungssteuerung mit Home Assistant zu realisieren, benötigst du folgende Hardware:

- **Smart Thermostate** (z. B. von Tado, Honeywell oder AVM Fritz!DECT)
- **Zentrale Steuerungseinheit** (Raspberry Pi, NUC oder Home Assistant Blue)
- **Temperatursensoren** (Optional, falls die Thermostate keine integrierten Sensoren haben)

## Benötigte Software

Für die Softwareseite wird folgendes benötigt:

- **Home Assistant** (aktuelle Version)
- **Integration für Smart Thermostate** (je nach Hersteller)
- **Automatisierungs-Skripte** für Zeitpläne und Anwesenheitserkennung
- **Home Assistant App** (optional, für mobile Steuerung)

---

## Installationsschritte

Im Folgenden findest du eine Schritt-für-Schritt-Anleitung, um die Heizungssteuerung in Home Assistant einzurichten:

1. **Installation von Home Assistant**  
   Installiere Home Assistant auf einem Raspberry Pi, NUC oder einer anderen kompatiblen Plattform. Die offizielle [Installationsanleitung](https://www.home-assistant.io/installation/) bietet dazu eine genaue Anleitung.

2. **Integration der Smart Thermostate**  
   Gehe in Home Assistant zu den Einstellungen und füge die entsprechende Integration für deine Thermostate hinzu. Folge den Schritten der jeweiligen Integration, um die Thermostate in Home Assistant einzubinden.

3. **Automatisierung erstellen**  
   Erstelle eine neue Automatisierung für die Heizungssteuerung. Du kannst basierend auf Zeitplänen oder Anwesenheitserkennung Regeln festlegen, z. B. die Heizung automatisch herunterzufahren, wenn niemand zu Hause ist.

4. **Anpassen der Steuerung über die Benutzeroberfläche**  
   Passe die Steuerung der Thermostate in der Home Assistant Benutzeroberfläche an und teste, ob alles wie gewünscht funktioniert.

5. **Mobile Steuerung einrichten**  
   Installiere die Home Assistant App auf deinem Smartphone, um die Heizung auch unterwegs zu steuern. So kannst du auch Benachrichtigungen über kritische Situationen, wie z. B. ein zu kaltes Zimmer, erhalten.

6. **Überprüfung und Feinjustierung**  
   Überprüfe nach ein paar Tagen, ob die Automatisierungen wie gewünscht laufen und passe sie nach Bedarf an, um den Energieverbrauch weiter zu optimieren.

---

Mit diesen Schritten kannst du die Heizungssteuerung in Home Assistant erfolgreich einrichten und verwalten. Die Automatisierungen helfen dir dabei, den Heizkomfort zu steigern und Energie zu sparen.