---
tags:
  - work-teco
  - to-do
content: TECOption, TECO Homeassistant; Databank; Zigbee; MQTT
style version: 1
---
Contacts:
- [[TECO Contacts#Dr. Till Riedel]]
- AIFB 
- [[KIT Other Contacts#Fabian Rybinski]]

### Status: Ongoing

Links:
- Server: [http://hass.teco.edu:8123](http://hass.teco.edu:8123)
	-  [http://10.11.46.42:8123](http://10.11.46.42:8123)
	- Alternative [http://10.11.46.45:8123](http://10.11.46.45:8123)
- Raspberry (Old): [http://homeassistant2.teco.edu:8123](http://homeassistant2.teco.edu:8123)


TO DO:
- [[#Permissions]]
- Look at NODE-RED
- Look at ESP-HOME
- [[#Door bell]]
- [[#Update Sonoff Firmware]]
- [[#Sonoff Smart Switch]]

Done: 
- [[#Home Assistant VM]]
- [[#MQTT]]
- [[#Zigbee2MQTT]]
- [[#Make backup]]
- [[#Databank]]
- [[#Install HACS]]
- [[#Printer]]

Resources Overview:
- User Specific Dashboard [Youtube](https://youtu.be/VvJdcY83hKE?si=qtTtsJOhxYgs7eqF)
- HACS [Youtube](https://youtu.be/Q8Gj0LiklRE?si=PMW2YTDFqt9CTCse)
- Templates [Youtube](https://youtu.be/cdz32TLu_gw?si=ucRy3m424vGPdiBA) and [Youtube](https://www.youtube.com/watch?v=ZeQJn7QVuiA)
- Layout stuff [Youtube](https://youtu.be/Ac02PzU17GQ?si=dcZ6C5Ani2F3SHJL)
- Permissions [HomeAssistant](https://developers.home-assistant.io/docs/auth_permissions/)
- [Tipps](https://youtu.be/FVusYP4fHFM?si=m2lXD2Ev--OoA9Lk)

---
## Meeting Raum

Contact: [[TECO Contacts#Alexander Studt]]

---
## Tasks
---


### Permissions
User permissions and management.
Disable uses from seeing certain elements.


**Status:** HOLD

Wait for: [[KIT Other Contacts#Fabian Rybinski]] 
	Study is in progress (finished in approx. 2-4 months; Jan-Mar)
	(Permissions not quite possible)

Look at groups 

Resources: 
- Permissions [HomeAssistant](https://developers.home-assistant.io/docs/auth_permissions/)
- User Specific Dashboard [Youtube](https://youtu.be/VvJdcY83hKE?si=qtTtsJOhxYgs7eqF)

---

### Home Assistant VM
Make Home Assistant
Supervised Version is needed!

**Status:** Done

Note:
- Server does not need Password if SSH is used!
- When loading Proxmox snapshot, restart hassio services manually!

To Do:
- ~~Major Update OS~~
	- [Tutorial](https://itsfoss.com/upgrade-ubuntu-version/)
	- [Ubuntu Names](https://askubuntu.com/questions/445487/what-debian-version-are-the-different-ubuntu-versions-based-on)
- ~~Install Supervised~~
- ~~Setup Hass~~
- ~~Ask Sirius for Domain~~
- Database!
- Add Minol
	- Python Script

Links:
- [VM Manual Intranet](https://intranet.teco.edu/projects/admin/wiki/VirtualMachines)
- [Home Assistant Installation](https://www.home-assistant.io/installation/linux)

Tutorial:
- [Ubuntu Supervised](https://pimylifeup.com/ubuntu-home-assistant/)(Dec. 2023)

Network Manager SOLUTION:
- [Manage](https://askubuntu.com/questions/882806/ethernet-device-not-managed)

Network Manager Resources:
- [Network Manager](https://developer-old.gnome.org/NetworkManager/stable/NetworkManager.conf.html)

Useful commands:
- `nmcli`
	- `sudo nmcli dev set eth0 managed yes`
- `nmtui`

---

### MQTT
Setup MQTT mosquito broker on server

**Status:** Done

Resources:
- [Home Assistant MQTT Install and Setup - A Beginner's Guide](https://www.youtube.com/watch?v=dqTn-Gk4Qeo)

---

### Zigbee2MQTT
Zigbee via MQTT

**Status:** Done

Resources:
- [Zigbee2MQTT](https://www.zigbee2mqtt.io/guide/getting-started/#installation)
- [Tutorial](https://www.youtube.com/watch?v=sFSqgiOoPMs) (Use Docker install)

---

### Make backup
Make backup of Home Assistant

⇒ Backups done by Backup of VM itself

**Status:** Done

Wait for: [[KIT Other Contacts#Fabian Rybinski]] 
	Sending resources

Do it with plugins or with raw configuration files

---

### Databank
Send data to Databank

**Status:** Done

⇒ Databank in VM

[[TECO Contacts#Tobias King]]
Setup on Server possible.

Ask [[KIT Other Contacts#Fabian Rybinski]] for resources.

Resources Overview:
- [MariaDB](https://www.youtube.com/watch?v=NgAs5CFLpks)
- [Home Assistant Recorder Docu](https://www.home-assistant.io/integrations/recorder/)
- [Maria DB Docker](https://mariadb.com/resources/blog/get-started-with-mariadb-using-docker-in-3-steps/)
	- [Maria DB Docker Image](https://hub.docker.com/_/mariadb)
- [Maria DB Docker compse](https://www.beekeeperstudio.io/blog/how-to-use-mariadb-with-docker)

Try potentially:
- [Advanced Home Assistant Connecting to an external database](https://www.youtube.com/watch?v=A-52iT43FQs)

#### Database Purge

[link](https://community.home-assistant.io/t/how-to-manually-use-recorder-purge-service/66738/3)

- Go to dev tools
- Go to servies
- Call Recorder.purge

#### Docker

Install command 1:
	`curl -fsSL get.docker.com | sh`

Install 2:
	[Docker Ubuntu](https://docs.docker.com/engine/install/ubuntu/)
	Note: For Raspberry Pi (64bit) use Debian Installation

List containers:
	`docker ps -a`

Start command:
	`docker compose up -d`
	(`--build` flag can be added)

Get logs:
	`docker logs [ID]`

Update with compose:
	[How to update existing images with docker-compose?](https://stackoverflow.com/questions/49316462/how-to-update-existing-images-with-docker-compose)

Resources:
- [Docker Volumes - Youtube](https://youtu.be/p2PH_YPCsis?si=j7TYN17MBCPwk61P)

---

### Install HACS
Install HACS

**Status:** DONE!

Resources Overview:
- HACS [Youtube](https://youtu.be/Q8Gj0LiklRE?si=PMW2YTDFqt9CTCse)

---

### Printer
Possibly add printers to monitoring!

**Status:** Done

Report back to Till

Resources:
- [Printer Card](https://community.home-assistant.io/t/ipp-printer-sensors-with-custom-button-card/250719)
	- Relies on [Custom-Cards](https://github.com/custom-cards/button-card)
		- Relies on HACS

---

### Update Sonoff Firmware
Update sonoff firmware of ZBDongle-E

**Status:** Ongoing

Resources:
- Guide: [Sonoff Zigbee 3.0 USB Dongle Plus - How to upgrade the firmware](https://www.youtube.com/watch?v=KBAGWBWBATg)
- [Online Flasher](https://darkxst.github.io/silabs-firmware-builder/)


---

### Sonoff Smart Switch
Add Sonoff Smart Switch (SONOFF ZBMINI)

**Status:** Ongoing

Note: By default, the switch ([SONOFF ZBMINI](https://www.zigbee2mqtt.io/devices/ZBMINI.html)) is not supported by zigbee2mqtt

Resources:
- (Obsolete) Add new device to devices.js
	- [How to add Sonoff ZB Mini to ZigBee2MQTT](https://www.youtube.com/watch?v=PQOIAsbGDqY)
- Official guide to add new device:
	- [Support new devices](https://www.zigbee2mqtt.io/advanced/support-new-devices/01_support_new_devices.html)
		- [Switch Example](https://github.com/Koenkk/zigbee2mqtt.io/blob/master/docs/externalConvertersExample/switch.js)

---

### Door bell
Integrate doorbell into HS.

**Status:** Ongoing

Info:
- Doorbell runs on 24V AC
- When switch is closed door bell rings
- Wire terminals are used

To do:
- Use Relais to close circuit
- Add ESP (ESP01) to monitor if doorbell is pressed
- Use door signal for relais directly!
- Maybe add manual bypass for relay (in case relay fails)

Resources:
- [ESP01 with Home Assistant](https://youtu.be/oYU0JMWwSuI?si=tAfujUPQCTUWsTvI)

---

# VMs

VM List
- Main VM
	- Homeassistant
- Second VM
	- Database
	- Minol App

## Main VM

Name: `debian@teco-homeassistant`
IP: `10.16.0.4`
SSH: `ssh debian@10.16.0.4`

HASS: Homeassistant Supervised
	Repo: [Homeassistant Supervised Github](https://github.com/home-assistant/supervised-installer)

Note: VM IP is on IoT net within TECO
## Second VM

Name: `ubuntu@teco-homeassistant`
IP: `10.11.46.73`
SSH: `ssh ubuntu@10.11.46.73`

Database: MariaDB
Apps:
- Minol App

(All active programs are managed by Docker)

Note: VM IP is on VM net within TECO


# Zigbee2Mqtt Management

Zigbee Devices:  [[TECO IoT - Zigbee Register]]

Coordinator: Conbee 2
Host device: Raspberry Pi

Name: `teco@zigbee-terminal`
IP: `10.16.0.2`
SSH: `ssh teco@10.16.0.2`