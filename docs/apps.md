# Anwendungen
Hier findest du verschiedene Anwendungsbeispiele, die detaillierter beschrieben werden. Klicke auf eine der folgenden Anwendungen, um mehr Informationen zu erhalten.

## Beispielanwendungen

1. [Beleuchtung](./apps/beleuchtung.md)
2. [Heizungssteuerung](./apps/heizungssteuerung.md)
3. [Feuermelder](./apps/feuermelder.md)
4. [Raumbelegung](./apps/raumbelegung.md)
5. [Gerätemanagement](./apps/geraetemanagement.md)
6. [Wetter](./apps/wetter.md)
7. [Anwesenheitserkennung](./apps/anwesenheit.md)
8. [Kaffeezähler](./apps/kaffee.md)

---

## Beleuchtung
Die Beleuchtungsautomatisierung ermöglicht es, Lichter basierend auf Tageszeit, Bewegungssensoren oder manuellem Eingriff zu steuern. [Mehr Details](./apps/beleuchtung.md)

## Heizungssteuerung
Die Heizungssteuerung optimiert den Energieverbrauch, indem sie die Temperatur automatisch an Anwesenheit und Zeitpläne anpasst. [Mehr Details](./apps/heizungssteuerung.md)

## Feuermelder
Die Sicherheitsüberwachung umfasst die Integration von Kameras, Bewegungsmeldern und Alarmanlagen für einen umfassenden Schutz. [Mehr Details](./apps/feuermelder.md)

## Raumbelegung
Steuere deine Musikgeräte zentral über Home Assistant, inklusive Lautstärkeregelung und Playlist-Management. [Mehr Details](./apps/raumbelegung.md)

## Gerätemanagement
Automatisiere deine Haushaltsgeräte und erhalte Benachrichtigungen über den Status deiner Geräte, wie Waschmaschinen oder Kühlschränke. [Mehr Details](./apps/geraetemanagement.md)

## Wetterinformationen
Erhalte aktuelle Wetterinformationen und erstelle benutzerdefinierte Benachrichtigungen basierend auf Wetterereignissen. [Mehr Details](./apps/wetter.md)

## Anwesenheitserkennung
Automatisiere deine Smart Home Geräte basierend auf der Erkennung, ob jemand zu Hause ist oder nicht. [Mehr Details](./apps/anwesenheit.md)

## Kaffeezähler
Erfasse die getrunkenen Tassen Kaffee. [Mehr Details](./apps/kaffee.md)

---

Jede der oben genannten Anwendungen enthält detaillierte Informationen, wie du sie in Home Assistant umsetzen kannst. Klicke einfach auf die jeweiligen Links, um zu den entsprechenden Seiten zu gelangen.